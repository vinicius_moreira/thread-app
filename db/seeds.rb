# ruby encoding: utf-8

# gerando publicações aleatórias
12.times do |n|
     
     # gerando conteúdo aleatório
     conteudoGerado = Faker::Lorem.sentence(word_count = 5, supplemental = false, random_words_to_add = 5)
    
     # criando uma publicação raiz
     pr = Publicacao.create!(conteudo: conteudoGerado)
    
     # gerando dois comentários, e uma resposta para o primeiro gerado
     12.times do |i|
        conteudoGerado = Faker::Lorem.sentence(word_count = 5, supplemental = false, random_words_to_add = 5)
         c1 = pr.publicacaos.build(conteudo: conteudoGerado)
         pr.save        

        if(i == 1)
          conteudoGerado = Faker::Lorem.sentence(word_count = 5, supplemental = false, random_words_to_add = 5)
          c1.publicacaos.build(conteudo: conteudoGerado)
          c1.save
        end
     end
end
