require 'test_helper'

class PublicacaoTest < ActiveSupport::TestCase
  
   def setup
      @publicacao = Publicacao.new(conteudo:"Teste");
   end

   test "Publicacao Válida" do
      assert @publicacao.valid?
   end

   test "Conteúdo deveria estar presente" do
      @publicacao.conteudo = "";
      assert_not @publicacao.valid?
   end

   test "Publicação Inválida: palavra restrita" do
      @publicacao.conteudo = "verde";
      assert_not @publicacao.valid?
   end

   test "Publicação Inválida: palavra restrita 'fora do padrão'" do
      @publicacao.conteudo = "vERde";
      assert_not @publicacao.valid?
   end

   test "Publicação Inválida: palavra restrita 'espaços'" do
      @publicacao.conteudo = "   vERde  ";
      assert_not @publicacao.valid?
   end

   test "Publicação Inválida: palavra restrita dentro de uma frase 'espaços'+'fora do padrão'" do
      @publicacao.conteudo = "Bola   vERde  ";
      assert_not @publicacao.valid?
   end

end
