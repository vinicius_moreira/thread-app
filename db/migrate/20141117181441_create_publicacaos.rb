class CreatePublicacaos < ActiveRecord::Migration
  def change
    create_table :publicacaos do |t|
      t.text :conteudo
      t.references :publicacao, index: true

      t.timestamps null: false
    end
  end
end
