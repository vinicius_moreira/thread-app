class PublicacaoValidator < ActiveModel::Validator
   INVALIDAS = %w(verde azul)

   def validate(publicacao)
	INVALIDAS.each do |invalida|
	   if(!publicacao.conteudo.downcase.match("(^|\s)#{invalida}(\s|$)").nil?)
		publicacao.errors[:base] << invalida
            end
         end
   end

end
