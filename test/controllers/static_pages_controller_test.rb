require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "Acessar a Home" do
    get :home
    assert_response :success
    assert_select "title", "Threads"
  end

end
