$( document ).ready(function() {

   window.comentar = function(element) {
  	publicacao_id = $(element).attr('id').split('$')[1];

        if($('#ta_reply_'+publicacao_id).length==0){
	  	// definindo 'id' da TextArea gerada = 'ta_reply_'+publicacao_id
		novof = "<br/><textarea id='ta_reply_:id'></textarea><input id='bt_reply_:id' type='button' value='Submit Query' onclick='enviar(this,:id)' />";
		novof = novof.replace(/:id/g,publicacao_id);
	  	$(novof).insertAfter($(element));
	}
   };   
  
   window.enviar = function (element, publicacao_id){
	url_path = $('#url_path').text()+"/"+publicacao_id;
	
       $.ajax({
		    method: "PATCH",
		    url: url_path,
		    data: { conteudo: $("#ta_reply_"+publicacao_id).val()},
		    success: function(publicacaoJSON){
                        if(publicacaoJSON.base == null){
		    		adicionarComentario(publicacaoJSON,publicacao_id);
			}
			else{
			    marcarPalavrasInvalidas(publicacaoJSON, publicacao_id);
			    alert('Você não pode publicar utilizando as palavras:'+publicacaoJSON.base);
			}
		   }
		});
   };

   var marcarPalavrasInvalidas = function(invalidasJSON, publicacao_id){
        invalidas = invalidasJSON.base; 
 	digitado = $("#ta_reply_"+publicacao_id).val().split(' ');

       for(i = 0; i < digitado.length; i++){
           for(j = 0; j < invalidas.length; j++){
		if(digitado[i].toUpperCase()==invalidas[j].toUpperCase()){
			digitado[i] = digitado[i].replace(/./g,'@');
			break;		
		}
	   }  
       }
 
       $("#ta_reply_"+publicacao_id).val(digitado.join(' '));
   };

   var adicionarComentario = function(publicacaoJSON, publicacaoId){
	comentarioPai = $("#ta_reply_"+publicacao_id).parent();
	limparFormulario(comentarioPai, publicacao_id);

        filhosExistentes = comentarioPai.find('ol').length == 1
        comentario = "";

        if(filhosExistentes){
		comentarioPai = comentarioPai.find('ol')[0]
	}
	else{
	        comentario += "<ol>";
	}
                        
        comentario += "<li>:p<input id='bt_reply$:pid' type='button' value='reply' onclick='comentar(this)' /></li>";
                        
	if(!filhosExistentes) comentario += "</ol>"			
		
	comentario = comentario.replace(":pid",publicacaoJSON.id);
        comentario = comentario.replace(":p", gerarRepresentacao(publicacaoJSON));  
	adicionar(comentario,comentarioPai,filhosExistentes);
   };

   var limparFormulario = function(comentarioPai, publicacao_id){
	$(comentarioPai.find('br')[0]).remove();
	$("#ta_reply_"+publicacao_id).remove();
	$("#bt_reply_"+publicacao_id).remove();		
   };

   var gerarRepresentacao = function(publicacaoJSON) {
	diaHora = publicacaoJSON.created_at.replace("T"," ");
        diaHora = diaHora.substr(0, diaHora.indexOf("."));

        return diaHora+" "+publicacaoJSON.conteudo;
   };

   var adicionar = function(comentario, comentarioPai, filhosExistentes){
	if(filhosExistentes){                       
 		$(comentarioPai).prepend(comentario);
	}
	else{
	        $(comentarioPai).append(comentario);
	}
   };
});
