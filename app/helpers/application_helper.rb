module ApplicationHelper

  # Retorna o título completo da página requisitada
  def full_title(page_title = '')
    base_title = "Threads"
    if page_title.empty?
      return base_title
    else
      return "#{page_title} | #{base_title}"
    end
  end
end
