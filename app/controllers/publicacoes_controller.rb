class PublicacoesController < ApplicationController
  
  def index
    # inicializando uma nova publicação
    @publicacao = Publicacao.new
    # buscando somente as publicações "raízes"
    @publicacoes = Publicacao.where(publicacao_id: nil).order('created_at DESC').paginate(page: params[:page], per_page: 10)
  end

  def show
     @publicacao = Publicacao.find(params[:id])
  end

  def create
     @publicacao = Publicacao.new(publicacao_params)
     @publicacao.save
     index
     render "index"
  end

  def update
     @publicacao = Publicacao.find(params[:id])
     nova = @publicacao.publicacaos.build(conteudo: params[:conteudo]);
     if(@publicacao.valid?)
	nova.save!
        return render json: nova
     end 
     return render json: nova.errors
  end

  private

    def publicacao_params
      params.require(:publicacao).permit(:conteudo)
    end
end
