module PublicacoesHelper

   # imprime uma publicação em formato de árvore
   def imprimir(publicacao)
      arvore = "";
      arvore << "<ol>" if publicacao.publicacao.nil?;
      arvore << "<li>#{publicacao}<input id='bt_reply$#{publicacao.id}' type='button' value='reply' onclick='comentar(this)' />";
      
      if !publicacao.publicacaos.empty?
          arvore << "<ol>" 
          filhos = ordenar(publicacao.publicacaos)
          filhos.each do |p|
             arvore << imprimir(p);
 	  end
          arvore << "</ol>"
      end 

      arvore << "</li>"
      arvore << "</ol>" if publicacao.publicacao.nil?;
      return arvore;
   end

   def ordenar(publicacoes)
	publicacoes.sort_by {|e| e.created_at}.reverse
   end
 
end
